const multer = require('multer')
const MIME_TYPES = {
    'image/jpeg':'jpeg',
    'image/jpg':'jpg',
    'image/png':'png'
}

//Création d'un objet de configuration de multer
const storage = multer.diskStorage({
    destination : (req, file, callback) => {
        callback(null, 'images')
    },
    // On génère le nouveau nom du fichier
    filename : (req, file, callback) => {
        const removeSpace = file.originalname.replace(' ','_');
        const extension = removeSpace.split(".").pop()
        const name = removeSpace.replace("." + extension,"")
        console.log({extension, name})
        callback(null, name + Date.now() + '.' + extension);
    }
})

module.exports = multer({storage}).single('image');