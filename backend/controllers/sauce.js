const Sauce = require('../models/Sauce')
const fs = require('fs')

//Création d'une sauce - route POST
exports.createSauce = (req, res, next) => {
  const sauceObject = JSON.parse(req.body.sauce);
  delete sauceObject._id;
  const sauce = new Sauce({
    ...sauceObject,
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  });
  sauce.likes = 0
  sauce.dislikes = 0
  sauce.usersLiked = []
  sauce.usersDisliked = []
  sauce.save()
    .then((savedSauce) => {

      res.status(201).json({
        message: 'Sauce enregistrée !'
      })
    })
    .catch(error => {

      res.status(400).json({
        error
      })
    });
}

//Modification d'une sauce - route PUT


//Code original
exports.updateSauce = (req, res, next) => {


  //suppression de l'image

  Sauce.findOne({
      _id: req.params.id
    })
    .then(sauce => {

      let sauceObject = {}
      if (req.file) {
        const fileName = sauce.imageUrl.split('/images/')[1]
        fs.unlink(`images/${fileName}`, () => {})
        sauceObject = {
          ...JSON.parse(req.body.sauce),
          imageUrl: `${req.protocol}://${req.get("host")}/images/${req.file.filename}`
        }

      } else {
        sauceObject = {
          ...req.body
        }
      }


      Sauce.updateOne({
          _id: req.params.id
        }, {
          ...sauceObject,
          _id: req.params.id
        })
        .then(() => {
          res.status(200).json({
            message: "Sauce modifiée"
          })
        })
        .catch(error => res.status(400).json({
          error
        }))
    })
    .catch(error => res.status(500).json({
      error
    }))



}

//Suppression d'une sauce - route DELETE
exports.deleteSauce = (req, res, next) => {
  Sauce.findOne({
      _id: req.params.id
    })
    .then((sauce) => {
      if (!sauce) {
        res.status(404).json({
          error: new Error('Sauce non trouvée')
        })
      }
      if (sauce.userId !== req.auth.userId) {
        return res.status(401).json({
          error: new Error('Requête non authorisé')
        })
      }
      const fileName = sauce.imageUrl.split('/images/')[1]
      fs.unlink(`images/${fileName}`, () => {
        Sauce.deleteOne({
            _id: req.params.id
          })
          .then(() => {
            res.status(201).json({
              message: 'Sauce supprimée'
            })
          })
          .catch(error => res.status(400).json({
            error
          }))
      })
    })
    .catch(error => res.stats(500).json({
      error
    }))
}

//Récupération d'une sauce - route GET
exports.getOneSauce = (req, res, next) => {
  Sauce.findOne({
      _id: req.params.id
    })
    .then(sauce => res.status(200).json(sauce))
    .catch(error => res.status(404).json({
      error
    }))
}

//Récupération de toutes les sauces - route GET
exports.getAllSauces = (req, res, next) => {
  Sauce.find()
    .then(sauces => res.status(200).json(sauces))
    .catch(error => res.status(400).json({
      error
    }))
}
//Liker une sauce
exports.likeSauce = (req, res, next) => {
  Sauce.findOne({
      _id: req.params.id
    })
    .then(sauce => {
      console.log(sauce, req.body, req.params);
      const likeValue = req.body.like
      const userId = req.body.userId
      switch (likeValue) {
        case 1:
          //check if like => break  
          if (sauce.usersLiked.includes(userId)) {
            break
          }
          //check if dislike => remove dislike && decrement Sauces.dislikes
          if (sauce.usersDisliked.includes(userId)) {
            const index = sauce.usersDisliked.indexOf(userId)
            sauce.usersDisliked.splice(index, 1)
            sauce.dislikes -= 1
          }
          //increment Sauce.likes && add user Sauce.usersLiked
          sauce.likes += 1
          sauce.usersLiked.push(userId)
          break;
        case 0:
          //check if dislike => remove dislike && decrement Sauces.dislikes
          if (sauce.usersDisliked.includes(userId)) {
            const index = sauce.usersDisliked.indexOf(userId)
            sauce.usersDisliked.splice(index, 1)
            sauce.dislikes -= 1
          }
          //check if like => remove like && decrement Sauces.likes
          if (sauce.usersLiked.includes(userId)) {
            const index = sauce.usersLiked.indexOf(userId)
            sauce.usersLiked.splice(index, 1)
            sauce.likes -= 1
          }
          break;
        case -1:
          //check if dislike => break  
          if (sauce.usersDisliked.includes(userId)) {
            break
          }
          //check if like => remove like && decrement Sauces.likes
          if (sauce.usersLiked.includes(userId)) {
            const index = sauce.usersLiked.indexOf(userId)
            sauce.usersLiked.splice(index, 1)
            sauce.likes -= 1
          }
          //increment Sauce.dislikes && add user Sauce.usersDisliked
          sauce.dislikes += 1
          sauce.usersDisliked.push(userId)
          break;
      }
      console.log(sauce);
      sauce.save()
        .then(sauce => {
          res.status(200).json({
            sauce,
            message: "Sauce likée"
          })
        })
        .catch(error => {
          res.stats(500).json({
            error
          })
        })
    })


}