const express = require('express')
const router = express.Router()
const sauceCtrl = require('../controllers/sauce')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer-config')

//Liste des routes
router.post('/',  auth, multer, sauceCtrl.createSauce); //Création d'une sauce. 
router.put('/:id',  auth, multer, sauceCtrl.updateSauce);//Met à jour la sauce avec l'_id fourni.
router.delete('/:id', auth, multer, sauceCtrl.deleteSauce); //Supprime la sauce avec l'_id fourni.
router.get('/:id',  auth, multer, sauceCtrl.getOneSauce) //Renvoie la sauce avec l’_id fourni.
router.get('/', auth, multer, sauceCtrl.getAllSauces); //Renvoie un tableau de toutes les sauces de la base de données.
router.post('/:id/like', auth, multer, sauceCtrl.likeSauce); //Définit le statut « Like » pour l' userId fourni


module.exports = router